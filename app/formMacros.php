<?php

Form::macro('indexInput', function($name, $text, $placeholder = null, $type = 'text', $error = null) {

	$label = Form::label($name, $text, array('class' => 'col-sm-3 control-label'));
	$input = Form::input($type, $name, null, array('placeholder'=>$placeholder, 'class' => 'form-control input-sm'));

	$formGroup = '<div class="form-group">';
	$errorMessage = '';


	if ($error != null && strlen($error) > 0) {
		$formGroup = '<div class="form-group has-error">';
		$errorMessage = '<p class="text-danger">'.$error.'</p>';
	}

	$item = $formGroup.
			$label.
			'<div class="col-sm-9">'.
				$input.
				$errorMessage.
			'</div>
		</div>';

	return $item;

});

Form::macro('editInput', function($name, $text, $placeholder = null, $type = 'text', $error = null) {

	$label = Form::label($name, $text, array('class' => 'col-sm-3 control-label'));
	$input = Form::input($type, $name, null, array('placeholder'=>$placeholder, 'class' => 'form-control input-sm'));

	$formGroup = '<div class="form-group">';
	$errorMessage = '';


	if ($error != null && strlen($error) > 0) {
		$formGroup = '<div class="form-group has-error">';
		$errorMessage = '<p class="text-danger">'.$error.'</p>';
	}

	$item = $formGroup.
			$label.
			'<div class="col-sm-9">'.
				$input.
				$errorMessage.
			'</div>
		</div>';

	return $item;

});

?>