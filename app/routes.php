<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Handle page errors
App::error(function($exception, $code) {
	switch ($code) {
		// case 403:
		// 	return Response::view('errors.403', array(), 403);

		case 404:
			return Response::view('errors.404', array(), 404);

		// case 500:
		// 	return Response::view('errors.500', array(), 500);

		// default:
		// 	return Response::view('errors.default', array(), $code);
	}
});


// GETS
Route::get('/', array('uses' => 'UserController@index'));
Route::get('index', array('uses' => 'HomeController@showIndex'));
Route::get('userLogout', array('uses' => 'HomeController@userLogout'));

// POSTS
Route::post('login', array('uses' => 'HomeController@userLogin'));
Route::post('register', array('uses' => 'UserController@create'));

// RESOURCES
Route::resource('users', 'UserController');
Route::resource('user', 'UserController');
