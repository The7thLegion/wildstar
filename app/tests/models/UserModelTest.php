<?php

class UserModelTest extends TestCase {

	public function setUp() {
		parent::setUp();
		$this->prepareTest();
	}

	public function test__userHasCharacters() {
		$user = User::first(); // Should be The7thLegion

		$this->checkUserData($user); // Checks that it's The7thLegion

		$this->assertNotNull($user->characters);

		$this->assertCount(1, $user->characters, 'More characters have been added, update count.');

		$this->assertInstanceOf('Character', $user->characters->first());
	}

	public function test__userDelete() {
		$user = User::first(); // Should be The7thLegion

		$this->checkUserData($user); // Checks that it's The7thLegion

		$user->delete();

		$user = User::where('username', '=', 'The7thLegion')->first();

		$this->assertNull($user);

		// Check that the user softdeleted. (Still in database)
		$deletedUser = User::withTrashed()->first();

		$this->checkUserData($deletedUser);

		$this->assertNotNull($deletedUser->deleted_at);

		// Check cascade deletes
		$character = Character::where('id', '=', $deletedUser->id)->first();
		$this->assertNull($character);

		$deletedCharacter = Character::withTrashed()->where('id', '=', $deletedUser->id)->first();
		$this->assertNotNull($deletedCharacter->deleted_at);

	}

}

?>