<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase {

	public $cleanData = true;

	/**
	 * Creates the application.
	 *
	 * @return \Symfony\Component\HttpKernel\HttpKernelInterface
	 */
	public function createApplication()
	{
		$unitTesting = true;

		$testEnvironment = 'testing';

		return require __DIR__.'/../../bootstrap/start.php';
	}

	// Place in the setUp() of each test class
	public function prepareTest() {

		// Do we want to clean the DB?
		if ($this->cleanData)
		{
			// Migrate and seed the database. (This will destroy everything else.)
			Artisan::call('migrate');
			Artisan::call('db:seed');
		}

		// Do not send any mail.
		Mail::pretend(true);
	}

	// Because we will always use the fixture data to test we can use this
	// in each test case.
	public function checkUserData($user) {
		// Check we have the user.
		$this->assertNotNull($user);
		// Check that the user is The7thLegion
		$this->assertEquals('The7thLegion', $user->username);
	}

	public function tearDown() {
		Artisan::call('migrate');
		Artisan::call('db:seed');
		Mail::pretend(false);
	}

}
