<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $softDelete = true;

	protected $fillable = array('website', 'about', 'gender', 'dob');
	protected $guarded = array('id', 'username', 'email', 'password');

	public $genderEnum = array(
		'Female' => 'Female',
		'Male' => 'Male'
	);

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier() {
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword() {
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail() {
		return $this->email;
	}

	public function characters() {
		return $this->hasMany('Character');
	}

	public function delete() {
		// Delete related user objects.
		if ($this->characters) {
			foreach($this->characters as $character) {
				$character->delete();
			}
		}

		// Delete user model.
		return parent::delete();
	}

}