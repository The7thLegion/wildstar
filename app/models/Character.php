<?php

class Character extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'characters';

	protected $softDelete = true;

	protected $fillable = array('name', 'server', 'race', 'faction', 'class', 'character_level', 'path', 'path_level');
	protected $guarded = array('id', 'user_id',);

	protected $serverEnum = array(
		'Server 1' => 'Server 1',
		'Server 2' => 'Server 2'
	);

	protected $raceEnum = array(
		'Aurin' => 'Aurin',
		'Cassian' => 'Cassian',
		'Chua' => 'Chua',
		'Drakken' => 'Drakken',
		'Granok' => 'Granok',
		'Human' => 'Human',
		'Mechari' => 'Mechari',
		'Mordesh' => 'Mordesh'
	);

	protected $factionEnum = array(
		'Dominion' => 'Dominion',
		'Exile' => 'Exile'
	);

	protected $pathEnum = array(
		'Explorer' => 'Explorer',
		'Scientist' => 'Scientist',
		'Settler' => 'Settler',
		'Solider' => 'Solider'
	);

	public function user() {
		$this->belongsTo('User');
	}

	public function tradeskills() {
		return $this->hasMany('Tradeskill');
	}

	public function hobbies() {
		return $this->hasMany('Hobbie');
	}

	public function delete() {
		// Delete related character objects.
		if ($this->tradeskills) {
			foreach($this->tradeskills as $tradeskill) {
				$tradeskill->delete();
			}
		}
		if ($this->hobbies) {
			foreach($this->hobbies as $hobbie) {
				$hobbie->delete();
			}
		}

		// Delete character model.
		return parent::delete();
	}

}