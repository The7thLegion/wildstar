<?php

class Hobbie extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'hobbies';

	protected $softDelete = true;

	protected $fillable = array('hobbie', 'level');
	protected $guarded = array('id', 'character_id',);

	protected $tradeskillEnum = array(
		'Cooking' => 'Cooking',
		'Farming' => 'Farming',
		'Fishing' => 'Fishing'
	);

	public function character() {
		$this->belongsTo('Character');
	}

}