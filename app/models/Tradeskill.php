<?php

class Tradeskill extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tradeskills';

	protected $softDelete = true;

	protected $fillable = array('tradeskill', 'level', 'active');
	protected $guarded = array('id', 'character_id',);

	protected $tradeskillEnum = array(
		'Architect' => 'Architect',
		'Armorer' => 'Armorer',
		'Miner' => 'Miner',
		'Outfitter' => 'Outfitter',
		'Relic hunter' => 'Relic hunter',
		'Survivalist' => 'Survivalist',
		'Tailor' => 'Tailor',
		'Technologist' => 'Technologist',
		'Weaponsmith' => 'Weaponsmith'
	);


	public function character() {
		$this->belongsTo('Character');
	}

}