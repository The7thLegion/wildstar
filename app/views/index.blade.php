@extends('layout')

@section('content')
	<h1>Welcome to my Wildstar fansite.</h1>
@stop

@section('sidebar')
	{{ Form::open(array('action' => 'HomeController@userLogin', 'class' => 'form-horizontal')) }}
		<div class="col-md-12">
			<h1>Login</h1>

			@if (Session::get('message'))
				<div class="alert alert-danger">{{ Session::get('message') }}</div>
			@endif

			{{ Form::indexInput('loginUsername', 'Username:', null, 'text', $errors->first('loginUsername')) }}

			{{ Form::indexInput('loginPassword', 'Password:', null, 'password', $errors->first('loginPassword')) }}

			{{ Form::submit('Login', array('class' => 'btn btn-primary')) }}
		</div>
	{{ Form::close() }}

	{{ Form::open(array('action' => 'UserController@create', 'class' => 'form-horizontal')) }}
		<div class="col-md-12">
			<h1>Register</h1>

			{{ Form::indexInput('username', 'Username:', null, 'text', $errors->first('username')) }}

			{{ Form::indexInput('email', 'Email:', null, 'email', $errors->first('email')) }}

			{{ Form::indexInput('password', 'Password:', null, 'password', $errors->first('password')) }}

			{{ Form::indexInput('confirmPassword', 'Confirm Password:', null, 'password', $errors->first('confirmPassword')) }}

			{{ Form::submit('Register', array('class' => 'btn btn-primary')) }}
		</div>
	{{ Form::close() }}
@stop