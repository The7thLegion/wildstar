@extends('layout')

@section('content')
	<h1>Permission Denied:</h1>
	<div class="alert alert-danger">{{ $error }}</div>
@stop