<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Wildstar</title>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>

		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">

		<!-- Optional theme -->
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">

		{{ HTML::style('css/styles.css') }}

		<!-- Latest compiled and minified JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div id="wrap">
			<div class="container header">
				<h1>Wildstar!</h1>
				<p>
					<a class="btn btn-warning btn-xs" href="{{ URL::to('/') }}">Home</a>
					@if (Auth::check())
						<a class="btn btn-warning btn-xs" href="{{ URL::to('userLogout') }}">Logout</a>
					@else
						<a class="btn btn-warning btn-xs" href="{{ URL::to('/') }}">Login</a>
					@endif
				</p>
				@if (Auth::check())
					<p>
						Welcome, {{Auth::user()->username}}!
					</p>
				@endif
			</div>

			<div id="main" class="container clear-top">
				<div class="row">
					<div class="col-md-8">
						@yield('content')
					</div>
					<div class="col-md-4">
						@yield('sidebar')
					</div>
				</div>
			</div>
		</div>

		<footer class="container footer">
			Created using Laravel 4.1 | Author: Jacob Van De Velde | &#169; 2014
		</footer>
	</body>
</html>