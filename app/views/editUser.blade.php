@extends('layout')

@section('content')
	<h1>Add character?</h1>
@stop

@section('sidebar')
	{{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
			<div class="form-group ">
				{{ Form::label('username', 'Username:', array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('username', null, array('class' => 'form-control input-sm', 'disabled' => 'disabled')) }}
				</div>
			</div>
			<div class="form-group ">
				{{ Form::label('email', 'Email:', array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('email', null, array('class' => 'form-control input-sm', 'disabled' => 'disabled')) }}
				</div>
			</div>
			<div class="form-group ">
				{{ Form::label('website', 'Website:', array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('website', null, array('class' => 'form-control input-sm')) }}
				</div>
			</div>
			<div class="form-group ">
				{{ Form::label('about', 'About:', array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::textarea('about', null, array('class' => 'form-control input-sm', 'row', '5')) }}
				</div>
			</div>
			<div class="form-group ">
				{{ Form::label('gender', 'Gender:', array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('gender', $user->genderEnum, null, array('class' => 'form-control input-sm')) }}
				</div>
			</div>
			<div class="form-group ">
				{{ Form::label('age', 'Age:', array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('age', null, array('class' => 'form-control input-sm')) }}
					<span class="text-danger">{{ Session::get('error') }}</span>
				</div>
			</div>
			<a class="btn btn-danger" href="/">Cancel</a>
			{{ Form::submit('Update user information', array('class' => 'btn btn-success')) }}
	{{ Form::close() }}
@stop
