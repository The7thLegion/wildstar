@extends('layout')

@section('content')
	<p>
		<a class="btn btn-primary" href="/users/{{$user->id}}/edit">Add character</a>
	</p>
	<h2>Your characters! </h2>
	@if (count($user->characters) > 0)
		@foreach ($user->characters as $character)
			<ul>
				<li>Name: {{ $character->name }}</li>
				<li>Server: {{ $character->server }}</li>
				<li>Race: {{ $character->race }}</li>
				<li>Faction: {{ $character->faction }}</li>
				<li>Class: {{ $character->class }}</li>
				<li>Character level: {{ $character->character_level }}</li>
				<li>Path: {{ $character->path }}</li>
				<li>Path: {{ $character->path_level }}</li>
				<li>Active Tradeskills:</li>
				@foreach ($character->tradeskills as $tradeskill)
					@if ($tradeskill->active)
						<ul>
							<li>{{$tradeskill->tradeskill}}</li>
							<ul>
								<li>Level: {{$tradeskill->level}}</li>
							</ul>
						</ul>
					@endif
				@endforeach
				<li>Hobbies:</li>
				@foreach ($character->hobbies as $hobbie)
					<ul>
						<li>{{$hobbie->hobbie}}</li>
						<ul>
							<li>Level: {{$hobbie->level}}</li>
						</ul>
					</ul>
				@endforeach
			</ul>
		@endforeach
	@else
		No characters added.
	@endif
@stop

@section('sidebar')
	<p>
		@if (Auth::user()->admin === '1')
			<a class="btn btn-primary" href="/users">Back</a>
		@endif
		<a class="btn btn-primary" href="/users/{{$user->id}}/edit">Edit</a>
	</p>
	<h2>Your profile! </h2>
	<ul>
		<li>Username: {{ $user->username }}</li>
		<li>Email: {{ $user->email }}</li>
		<li>Website: {{ $user->website }}</li>
		<li>About: {{ $user->about }}</li>
		<li>Gender: {{ $user->gender }}</li>
		@if ($user->age !== NULL)
			<li>Age: {{ $user->age }}</li>
		@endif
	</ul>
@stop