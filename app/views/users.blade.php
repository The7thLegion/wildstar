@extends('layout')

@section('content')
	<h1>Hi boss, you are awesome!</h1>
@stop

@section('sidebar')
	<h2> List of users: </h2>
	@foreach ($users as $user)
		<h3><a href="/users/{{ $user->id }}">View {{ $user->username }}'s profile</a></h3>
	@endforeach

@stop