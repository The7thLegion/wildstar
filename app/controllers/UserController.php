<?php

class UserController extends BaseController {

	public function index() {

		if ($this->getRole() === 'ADMIN') {
			$users = User::all();
			return View::make('users')
				->with('users', $users);

		} else if ($this->getRole() === 'USER') {
			return View::make('user')
				->with('user', Auth::user());

		} else {
			return View::make('index');

		}

	}


	public function create() {

		$rules = array(
			'username' => 'required|unique:users|between:3,30',
			'email' => 'required|unique:users|email',
			'password' => 'required|alphaNum|between:5,20',
			'confirmPassword' => 'required|same:password'
		);

		$messages = array(
			'username.required' => 'The Username field is required.',
			'username.unique' => 'The Username has already been taken.',
			'username.between' => 'The Username must be between :min and :max characters.',
			'email.required' => 'The Email field is required.',
			'email.unique' => 'This email is already registered.',
			'password.required' => 'The Password field is required.',
			'password.alphaNum' => 'The password may only contain letters and numbers.',
			'password.between' => 'The Password must be between :min and :max characters.',
			'confirmPassword.required' => 'The Confirm Password and Password must match.',
			'confirmPassword.same' => 'The Confirm Password and Password must match.'
		);

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails()) {
			return View::make('index')
				->withErrors($validator)
				->withInput(Input::except('registerPassword', 'confirmRegisterPassword'));

		}

		die('Gets here!');

		$user = new User;

		$user->save();

		Log::info('New user', array('Username' => $user->username));

		return Redirect::to('users');

	}


	public function show($id) {

		$user = User::find($id);

		if ($this->getRole($user)) {
			return View::make('user')
				->with('user', $user);

		} else {
			$this->logEvent('alert', 'Permission Denied', 'User Controller: show('.$id.')');
			return View::make('errors.permissionDenied', array('error' => 'You do not have permission to VIEW this User.'));

		}

	}


	public function edit($id) {

		$user = User::find($id);
		if ($this->getRole($user)) {
			return View::make('editUser')
				->with('user', $user);

		} else {
			$this->logEvent('alert', 'Permission Denied', 'User Controller: edit('.$id.')');
			return View::make('errors.permissionDenied', array('error' => 'You do not have permission to EDIT this User.'));

		}

	}


	public function update($id)	{

		$user = User::find($id);

		if ($this->getRole($user)) {

			$rules = array(
				'age' => 'integer|digits:2'
			);

			$validator = Validator::make(Input::all(), $rules);

			if ($validator->fails()) {
				return Redirect::to('users/'.$user->id.'/edit')
					->with('user', $user)
					->with('error', 'User age must two digits or empty.');
			}

			$user->website = Input::get('website');
			$user->about = Input::get('about');
			$user->gender = Input::get('gender');
			$user->age = Input::get('age') === '' ? null : Input::get('age');

			$user->save();

			return Redirect::to('users/'.$user->id)
				->with('success', 'User updated successfully.');

		} else {
			$this->logEvent('alert', 'Permission Denied', 'User Controller: update('.$id.')');
			return View::make('errors.permissionDenied', array('error' => 'You do not have permission to UPDATE this User.'));

		}

	}

}

?>