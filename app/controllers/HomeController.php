<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showIndex() {

		if (Auth::check()) {
			return Redirect::to('user');
		}

		return View::make('index');
	}


	public function userLogin() {
		// Validate that username and password are not empty.
		$rules = array(
			'loginUsername' => 'required',
			'loginPassword' => 'required'
		);

		$messages = array(
			'loginUsername.required' => 'The Username field is required.',
			'loginPassword.required' => 'The Password field is required.'
		);

		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails()) {
			return View::make('index')
					->withErrors($validator)
					->withInput(Input::except('password'));
		}

		$userData = array(
			'username' => Input::get('loginUsername'),
			'password' => Input::get('loginPassword')
		);

		if (Auth::attempt($userData)) {
			return Redirect::action('UserController@index');
		} else {
			return Redirect::to('index')
				->with('message', 'Username or password is incorrect.');
		}
	}

	public function userLogout() {
		Session::flush(); // clear the session
		return Redirect::to('/'); // redirect the user to the login screen
	}

}