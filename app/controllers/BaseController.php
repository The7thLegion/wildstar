<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout() {
		if ( ! is_null($this->layout)) {
			$this->layout = View::make($this->layout);
		}
	}

	public function getRole($user = false) {
		if (Auth::check()) {

			 if (is_null($user)) {
			 	// Trying to do something on an entity that does not exist.
			 	$this->logEvent('alert', 'Entity Not Found', 'BaseController: getRole($user) - '.Request::url());
				return false;
			 }

			if (Auth::user()->admin === '1') {
				return 'ADMIN';
			}

			if ($user !== false) {
				if (Auth::user()->id == $user->id) {
					return 'OWNER';

				} else {
					// Used to direct people who are not the owner away.
					return false;

				}
			}

			return 'USER';
		}

		// Used to direct people not logged in away.
		return false;
	}


	public function logEvent($logType, $logMessage, $location) {

		$logArray = array(
			'user' => 'Guest; Not logged in',
			'location' => $location
		);

		if (Auth::check()) {
			$logArray['id'] = Auth::user()->id;
			$logArray['user'] = Auth::user()->username;
			$logArray['email'] = Auth::user()->email;
		}

		switch($logType){
			case('alert'):
				return Log::alert($logMessage, $logArray);
			break;

			case('warning'):
				return Log::warning($logMessage, $logArray);
			break;

			case('error'):
				return Log::error($logMessage, $logArray);
			break;

			default:
				// Default to info.
				return Log::info($logMessage, $logArray);

		}

	}

}