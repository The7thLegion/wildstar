<?php

use Illuminate\Database\Migrations\Migration;

class CreateAllTables extends Migration {

	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::create('users', function($table)
		{
			$table->increments('id');
			$table->string('username')->unique();
			$table->string('email')->unique();
			$table->string('password');
			$table->string('website')->nullable();
			$table->text('about')->nullable();
			$table->enum('gender', array('Female', 'Male'));
			$table->smallInteger('age')->nullable();
			$table->boolean('admin');

			// Attached to all tables. created_at, updated_at, deleted_at
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('characters', function($table)
		{
			$table->increments('id');
			$table->unsignedInteger('user_id');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->string('name', 64);
			$table->enum('server', array('Server 1', 'Server 2'));
			$table->enum('race', array('Aurin', 'Cassian', 'Chua', 'Drakken', 'Granok', 'Human', 'Mechari', 'Mordesh'));
			$table->enum('faction', array('Dominion', 'Exile'));
			$table->enum('class', array('Esper', 'Engineer', 'Medic', 'Spellslinger', 'Stalker', 'Warrior'));
			$table->smallInteger('character_level');
			$table->enum('path', array('Explorer', 'Scientist', 'Settler', 'Solider'));
			$table->smallInteger('path_level');

			// Attached to all tables. created_at, updated_at, deleted_at
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('tradeskills', function($table)
		{
			$table->increments('id');
			$table->unsignedInteger('character_id');
			$table->foreign('character_id')->references('id')->on('characters')->onDelete('cascade');
			$table->enum('tradeskill', array('Architect', 'Armorer', 'Miner', 'Outfitter', 'Relic hunter', 'Survivalist', 'Tailor', 'Technologist', 'Weaponsmith'));
			$table->smallInteger('level');
			$table->boolean('active');

			// Attached to all tables. created_at, updated_at, deleted_at
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('hobbies', function($table)
		{
			$table->increments('id');
			$table->unsignedInteger('character_id');
			$table->foreign('character_id')->references('id')->on('characters')->onDelete('cascade');
			$table->enum('hobbie', array('Cooking', 'Farming', 'Fishing'));
			$table->smallInteger('level');

			// Attached to all tables. created_at, updated_at, deleted_at
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hobbies');
		Schema::drop('tradeskills');
		Schema::drop('characters');
		Schema::drop('users');
	}

}