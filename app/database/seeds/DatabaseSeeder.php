<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsersTableSeeder');

		$this->call('CharactersTableSeeder');

		$this->call('TradeskillsTableSeeder');

		$this->call('HobbiesTableSeeder');

		$this->command->info('All tables seeded!');
	}


}


class UsersTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

		DB::table('users')->delete();

		// Starts the index back to zero.
		DB::table('users')->truncate();

		$user = new User;

		$user->fill(array(
			'username' => 'The7thLegion',
			'email' => 'the7thlegion@gmail.com',
			'password' => Hash::make('asd123'),
			'website' => 'http://the7thlegion.com/',
			'gender' => 'Male',
			'about' => 'Author and creator of this wonderful site!',
			'age' => 23,
			'admin' => true
		));

		$user->save();

		$user = new User;

		$user->fill(array(
			'username' => 'Noigel',
			'email' => 'noigel@gmail.com',
			'password' => Hash::make('asd123'),
			'website' => 'http://the7thlegion.com/',
			'gender' => 'Female',
			'about' => 'Alter ego of The7thLegio',
			'age' => 19,
			'admin' => false
		));

		$user->save();

		DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
	}

}


class CharactersTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

		DB::table('characters')->delete();

		// Starts the index back to zero.
		DB::table('characters')->truncate();

		// Get related User.
		$user = User::where('username', '=', 'The7thLegion')->firstOrFail();

		$character = new Character;

		$character->fill(array(
			'user_id' => $user->id,
			'name' => 'The7thLegion',
			'server' => 'Server 1',
			'race' => 'Mordesh',
			'faction' => 'Exile',
			'class' => 'Spellslinger',
			'character_level' => 1,
			'path' => 'Scientist',
			'path_level' => 1
		));

		$character->save();

		DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
	}

}


class TradeskillsTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

		DB::table('tradeskills')->delete();

		// Starts the index back to zero.
		DB::table('tradeskills')->truncate();

		// Get related Character.
		$character = Character::where('name', '=', 'The7thLegion')->firstOrFail();

		$tradeskill_one = new Tradeskill;
		$tradeskill_two = new Tradeskill;

		$tradeskill_one->fill(
			array(
				'character_id' => $character->id,
				'tradeskill' => 'Tailor',
				'level' => 1,
				'active' => true
			)
		);

		$tradeskill_two->fill(
			array(
				'character_id' => $character->id,
				'tradeskill' => 'Relic hunter',
				'level' => 1,
				'active' => true
			)
		);

		$tradeskill_one->save();
		$tradeskill_two->save();

		DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
	}

}


class HobbiesTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

		DB::table('hobbies')->delete();

		// Starts the index back to zero.
		DB::table('hobbies')->truncate();

		// Get related Character.
		$character = Character::where('name', '=', 'The7thLegion')->firstOrFail();

		$hobbie_one = new Hobbie;
		$hobbie_two = new Hobbie;

		$hobbie_one->fill(
			array(
				'character_id' => $character->id,
				'hobbie' => 'cooking',
				'level' => '1'
			)
		);

		$hobbie_two->fill(
			array(
				'character_id' => $character->id,
				'hobbie' => 'farming',
				'level' => '1'
			)
		);

		$hobbie_one->save();
		$hobbie_two->save();

		DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
	}

}
